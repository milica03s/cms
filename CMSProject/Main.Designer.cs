﻿namespace CMSProject
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabProdOrder = new System.Windows.Forms.TabPage();
            this.gbProdOrderAction = new System.Windows.Forms.GroupBox();
            this.btSubmitProdORder = new System.Windows.Forms.Button();
            this.tbProdORderMachineNo = new System.Windows.Forms.TextBox();
            this.tbProdORderPRodSpan = new System.Windows.Forms.TextBox();
            this.tbProdORderQProduced = new System.Windows.Forms.TextBox();
            this.cbProdORderUnit = new System.Windows.Forms.ComboBox();
            this.cbProdORderProduct = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.gbAction = new System.Windows.Forms.GroupBox();
            this.rbAddEntry = new System.Windows.Forms.RadioButton();
            this.tabSale = new System.Windows.Forms.TabPage();
            this.quantityerror = new System.Windows.Forms.Label();
            this.tbAvailable = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tbPricePerUnit = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cbSaleProduction = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cbCompany = new System.Windows.Forms.ComboBox();
            this.cbMarket = new System.Windows.Forms.ComboBox();
            this.btSubmitSale = new System.Windows.Forms.Button();
            this.tbSaleQuantity = new System.Windows.Forms.TextBox();
            this.cbSaleUnit = new System.Windows.Forms.ComboBox();
            this.cbSaleProduct = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tabActionTrail = new System.Windows.Forms.TabPage();
            this.dgActionTrail = new System.Windows.Forms.DataGridView();
            this.tabSummaryReports = new System.Windows.Forms.TabPage();
            this.gbReportData = new System.Windows.Forms.GroupBox();
            this.dgvSummary = new System.Windows.Forms.DataGridView();
            this.gbReportType = new System.Windows.Forms.GroupBox();
            this.btSummary = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.rbStock = new System.Windows.Forms.RadioButton();
            this.rbSaleByDay = new System.Windows.Forms.RadioButton();
            this.rbProdByDay = new System.Windows.Forms.RadioButton();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.homeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl.SuspendLayout();
            this.tabProdOrder.SuspendLayout();
            this.gbProdOrderAction.SuspendLayout();
            this.gbAction.SuspendLayout();
            this.tabSale.SuspendLayout();
            this.tabActionTrail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgActionTrail)).BeginInit();
            this.tabSummaryReports.SuspendLayout();
            this.gbReportData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSummary)).BeginInit();
            this.gbReportType.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.tabProdOrder);
            this.tabControl.Controls.Add(this.tabSale);
            this.tabControl.Controls.Add(this.tabActionTrail);
            this.tabControl.Controls.Add(this.tabSummaryReports);
            this.tabControl.Location = new System.Drawing.Point(12, 27);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(694, 314);
            this.tabControl.TabIndex = 0;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // tabProdOrder
            // 
            this.tabProdOrder.Controls.Add(this.gbProdOrderAction);
            this.tabProdOrder.Controls.Add(this.gbAction);
            this.tabProdOrder.Location = new System.Drawing.Point(4, 22);
            this.tabProdOrder.Name = "tabProdOrder";
            this.tabProdOrder.Padding = new System.Windows.Forms.Padding(3);
            this.tabProdOrder.Size = new System.Drawing.Size(686, 288);
            this.tabProdOrder.TabIndex = 0;
            this.tabProdOrder.Text = "Production Order";
            this.tabProdOrder.UseVisualStyleBackColor = true;
            // 
            // gbProdOrderAction
            // 
            this.gbProdOrderAction.Controls.Add(this.btSubmitProdORder);
            this.gbProdOrderAction.Controls.Add(this.tbProdORderMachineNo);
            this.gbProdOrderAction.Controls.Add(this.tbProdORderPRodSpan);
            this.gbProdOrderAction.Controls.Add(this.tbProdORderQProduced);
            this.gbProdOrderAction.Controls.Add(this.cbProdORderUnit);
            this.gbProdOrderAction.Controls.Add(this.cbProdORderProduct);
            this.gbProdOrderAction.Controls.Add(this.label6);
            this.gbProdOrderAction.Controls.Add(this.label5);
            this.gbProdOrderAction.Controls.Add(this.label4);
            this.gbProdOrderAction.Controls.Add(this.label3);
            this.gbProdOrderAction.Controls.Add(this.label2);
            this.gbProdOrderAction.Location = new System.Drawing.Point(6, 57);
            this.gbProdOrderAction.Name = "gbProdOrderAction";
            this.gbProdOrderAction.Size = new System.Drawing.Size(674, 225);
            this.gbProdOrderAction.TabIndex = 3;
            this.gbProdOrderAction.TabStop = false;
            // 
            // btSubmitProdORder
            // 
            this.btSubmitProdORder.Location = new System.Drawing.Point(138, 180);
            this.btSubmitProdORder.Name = "btSubmitProdORder";
            this.btSubmitProdORder.Size = new System.Drawing.Size(100, 23);
            this.btSubmitProdORder.TabIndex = 12;
            this.btSubmitProdORder.Text = "Submit";
            this.btSubmitProdORder.UseVisualStyleBackColor = true;
            this.btSubmitProdORder.Click += new System.EventHandler(this.btSubmitProdORder_Click);
            // 
            // tbProdORderMachineNo
            // 
            this.tbProdORderMachineNo.Location = new System.Drawing.Point(138, 133);
            this.tbProdORderMachineNo.Name = "tbProdORderMachineNo";
            this.tbProdORderMachineNo.Size = new System.Drawing.Size(100, 20);
            this.tbProdORderMachineNo.TabIndex = 11;
            // 
            // tbProdORderPRodSpan
            // 
            this.tbProdORderPRodSpan.Location = new System.Drawing.Point(138, 107);
            this.tbProdORderPRodSpan.Name = "tbProdORderPRodSpan";
            this.tbProdORderPRodSpan.Size = new System.Drawing.Size(100, 20);
            this.tbProdORderPRodSpan.TabIndex = 10;
            // 
            // tbProdORderQProduced
            // 
            this.tbProdORderQProduced.Location = new System.Drawing.Point(138, 81);
            this.tbProdORderQProduced.Name = "tbProdORderQProduced";
            this.tbProdORderQProduced.Size = new System.Drawing.Size(100, 20);
            this.tbProdORderQProduced.TabIndex = 9;
            // 
            // cbProdORderUnit
            // 
            this.cbProdORderUnit.FormattingEnabled = true;
            this.cbProdORderUnit.Location = new System.Drawing.Point(138, 54);
            this.cbProdORderUnit.Name = "cbProdORderUnit";
            this.cbProdORderUnit.Size = new System.Drawing.Size(121, 21);
            this.cbProdORderUnit.TabIndex = 7;
            // 
            // cbProdORderProduct
            // 
            this.cbProdORderProduct.FormattingEnabled = true;
            this.cbProdORderProduct.Location = new System.Drawing.Point(138, 27);
            this.cbProdORderProduct.Name = "cbProdORderProduct";
            this.cbProdORderProduct.Size = new System.Drawing.Size(121, 21);
            this.cbProdORderProduct.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 136);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Machine number:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 110);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Production span:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Quantity produced:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Unit:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Product:";
            // 
            // gbAction
            // 
            this.gbAction.Controls.Add(this.rbAddEntry);
            this.gbAction.Location = new System.Drawing.Point(6, 6);
            this.gbAction.Name = "gbAction";
            this.gbAction.Size = new System.Drawing.Size(674, 45);
            this.gbAction.TabIndex = 1;
            this.gbAction.TabStop = false;
            this.gbAction.Text = "Action:";
            // 
            // rbAddEntry
            // 
            this.rbAddEntry.AutoSize = true;
            this.rbAddEntry.Checked = true;
            this.rbAddEntry.Location = new System.Drawing.Point(18, 19);
            this.rbAddEntry.Name = "rbAddEntry";
            this.rbAddEntry.Size = new System.Drawing.Size(70, 17);
            this.rbAddEntry.TabIndex = 1;
            this.rbAddEntry.TabStop = true;
            this.rbAddEntry.Text = "Add entry";
            this.rbAddEntry.UseVisualStyleBackColor = true;
            // 
            // tabSale
            // 
            this.tabSale.Controls.Add(this.quantityerror);
            this.tabSale.Controls.Add(this.tbAvailable);
            this.tabSale.Controls.Add(this.label14);
            this.tabSale.Controls.Add(this.tbPricePerUnit);
            this.tabSale.Controls.Add(this.label13);
            this.tabSale.Controls.Add(this.cbSaleProduction);
            this.tabSale.Controls.Add(this.label12);
            this.tabSale.Controls.Add(this.cbCompany);
            this.tabSale.Controls.Add(this.cbMarket);
            this.tabSale.Controls.Add(this.btSubmitSale);
            this.tabSale.Controls.Add(this.tbSaleQuantity);
            this.tabSale.Controls.Add(this.cbSaleUnit);
            this.tabSale.Controls.Add(this.cbSaleProduct);
            this.tabSale.Controls.Add(this.label7);
            this.tabSale.Controls.Add(this.label8);
            this.tabSale.Controls.Add(this.label9);
            this.tabSale.Controls.Add(this.label10);
            this.tabSale.Controls.Add(this.label11);
            this.tabSale.Location = new System.Drawing.Point(4, 22);
            this.tabSale.Name = "tabSale";
            this.tabSale.Padding = new System.Windows.Forms.Padding(3);
            this.tabSale.Size = new System.Drawing.Size(686, 288);
            this.tabSale.TabIndex = 1;
            this.tabSale.Text = "Sale";
            this.tabSale.UseVisualStyleBackColor = true;
            this.tabSale.Click += new System.EventHandler(this.tabSale_Click);
            // 
            // quantityerror
            // 
            this.quantityerror.AutoSize = true;
            this.quantityerror.ForeColor = System.Drawing.Color.Coral;
            this.quantityerror.Location = new System.Drawing.Point(278, 146);
            this.quantityerror.Name = "quantityerror";
            this.quantityerror.Size = new System.Drawing.Size(41, 13);
            this.quantityerror.TabIndex = 34;
            this.quantityerror.Text = "label15";
            this.quantityerror.Visible = false;
            // 
            // tbAvailable
            // 
            this.tbAvailable.Location = new System.Drawing.Point(413, 116);
            this.tbAvailable.Name = "tbAvailable";
            this.tbAvailable.Size = new System.Drawing.Size(121, 20);
            this.tbAvailable.TabIndex = 33;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(278, 119);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(129, 13);
            this.label14.TabIndex = 32;
            this.label14.Text = "Available from production:";
            // 
            // tbPricePerUnit
            // 
            this.tbPricePerUnit.Location = new System.Drawing.Point(147, 87);
            this.tbPricePerUnit.Name = "tbPricePerUnit";
            this.tbPricePerUnit.Size = new System.Drawing.Size(121, 20);
            this.tbPricePerUnit.TabIndex = 31;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(25, 90);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 13);
            this.label13.TabIndex = 30;
            this.label13.Text = "Price per unit:";
            // 
            // cbSaleProduction
            // 
            this.cbSaleProduction.FormattingEnabled = true;
            this.cbSaleProduction.Location = new System.Drawing.Point(147, 116);
            this.cbSaleProduction.Name = "cbSaleProduction";
            this.cbSaleProduction.Size = new System.Drawing.Size(121, 21);
            this.cbSaleProduction.TabIndex = 29;
            this.cbSaleProduction.SelectedIndexChanged += new System.EventHandler(this.cbSaleProduction_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(25, 119);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 13);
            this.label12.TabIndex = 28;
            this.label12.Text = "Production:";
            // 
            // cbCompany
            // 
            this.cbCompany.FormattingEnabled = true;
            this.cbCompany.Location = new System.Drawing.Point(147, 198);
            this.cbCompany.Name = "cbCompany";
            this.cbCompany.Size = new System.Drawing.Size(121, 21);
            this.cbCompany.TabIndex = 27;
            // 
            // cbMarket
            // 
            this.cbMarket.FormattingEnabled = true;
            this.cbMarket.Location = new System.Drawing.Point(147, 169);
            this.cbMarket.Name = "cbMarket";
            this.cbMarket.Size = new System.Drawing.Size(121, 21);
            this.cbMarket.TabIndex = 26;
            this.cbMarket.SelectedIndexChanged += new System.EventHandler(this.cbMarket_SelectedIndexChanged);
            // 
            // btSubmitSale
            // 
            this.btSubmitSale.Location = new System.Drawing.Point(147, 234);
            this.btSubmitSale.Name = "btSubmitSale";
            this.btSubmitSale.Size = new System.Drawing.Size(100, 23);
            this.btSubmitSale.TabIndex = 25;
            this.btSubmitSale.Text = "Submit";
            this.btSubmitSale.UseVisualStyleBackColor = true;
            this.btSubmitSale.Click += new System.EventHandler(this.btSubmitSale_Click);
            // 
            // tbSaleQuantity
            // 
            this.tbSaleQuantity.Location = new System.Drawing.Point(147, 143);
            this.tbSaleQuantity.Name = "tbSaleQuantity";
            this.tbSaleQuantity.Size = new System.Drawing.Size(121, 20);
            this.tbSaleQuantity.TabIndex = 22;
            // 
            // cbSaleUnit
            // 
            this.cbSaleUnit.FormattingEnabled = true;
            this.cbSaleUnit.Location = new System.Drawing.Point(147, 60);
            this.cbSaleUnit.Name = "cbSaleUnit";
            this.cbSaleUnit.Size = new System.Drawing.Size(121, 21);
            this.cbSaleUnit.TabIndex = 20;
            this.cbSaleUnit.SelectedIndexChanged += new System.EventHandler(this.cbSaleUnit_SelectedIndexChanged);
            // 
            // cbSaleProduct
            // 
            this.cbSaleProduct.FormattingEnabled = true;
            this.cbSaleProduct.Location = new System.Drawing.Point(147, 33);
            this.cbSaleProduct.Name = "cbSaleProduct";
            this.cbSaleProduct.Size = new System.Drawing.Size(121, 21);
            this.cbSaleProduct.TabIndex = 19;
            this.cbSaleProduct.SelectedIndexChanged += new System.EventHandler(this.cbSaleProduct_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 201);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "Company:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 172);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Market:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 146);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Quantity sold:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(25, 63);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "Unit:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(25, 36);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 13);
            this.label11.TabIndex = 14;
            this.label11.Text = "Product:";
            // 
            // tabActionTrail
            // 
            this.tabActionTrail.Controls.Add(this.dgActionTrail);
            this.tabActionTrail.Location = new System.Drawing.Point(4, 22);
            this.tabActionTrail.Name = "tabActionTrail";
            this.tabActionTrail.Size = new System.Drawing.Size(686, 288);
            this.tabActionTrail.TabIndex = 2;
            this.tabActionTrail.Text = "ActionTrail";
            this.tabActionTrail.UseVisualStyleBackColor = true;
            // 
            // dgActionTrail
            // 
            this.dgActionTrail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgActionTrail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgActionTrail.Location = new System.Drawing.Point(3, 3);
            this.dgActionTrail.Name = "dgActionTrail";
            this.dgActionTrail.Size = new System.Drawing.Size(680, 282);
            this.dgActionTrail.TabIndex = 0;
            // 
            // tabSummaryReports
            // 
            this.tabSummaryReports.Controls.Add(this.gbReportData);
            this.tabSummaryReports.Controls.Add(this.gbReportType);
            this.tabSummaryReports.Location = new System.Drawing.Point(4, 22);
            this.tabSummaryReports.Name = "tabSummaryReports";
            this.tabSummaryReports.Size = new System.Drawing.Size(686, 288);
            this.tabSummaryReports.TabIndex = 3;
            this.tabSummaryReports.Text = "Summary Reports";
            // 
            // gbReportData
            // 
            this.gbReportData.Controls.Add(this.dgvSummary);
            this.gbReportData.Location = new System.Drawing.Point(150, 14);
            this.gbReportData.Name = "gbReportData";
            this.gbReportData.Size = new System.Drawing.Size(533, 271);
            this.gbReportData.TabIndex = 1;
            this.gbReportData.TabStop = false;
            // 
            // dgvSummary
            // 
            this.dgvSummary.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSummary.Location = new System.Drawing.Point(0, 9);
            this.dgvSummary.Name = "dgvSummary";
            this.dgvSummary.Size = new System.Drawing.Size(527, 262);
            this.dgvSummary.TabIndex = 0;
            // 
            // gbReportType
            // 
            this.gbReportType.Controls.Add(this.btSummary);
            this.gbReportType.Controls.Add(this.dateTimePicker1);
            this.gbReportType.Controls.Add(this.rbStock);
            this.gbReportType.Controls.Add(this.rbSaleByDay);
            this.gbReportType.Controls.Add(this.rbProdByDay);
            this.gbReportType.Location = new System.Drawing.Point(4, 4);
            this.gbReportType.Name = "gbReportType";
            this.gbReportType.Size = new System.Drawing.Size(140, 281);
            this.gbReportType.TabIndex = 0;
            this.gbReportType.TabStop = false;
            this.gbReportType.Text = "Summary:";
            // 
            // btSummary
            // 
            this.btSummary.Location = new System.Drawing.Point(59, 252);
            this.btSummary.Name = "btSummary";
            this.btSummary.Size = new System.Drawing.Size(75, 23);
            this.btSummary.TabIndex = 3;
            this.btSummary.Text = "Summary";
            this.btSummary.UseVisualStyleBackColor = true;
            this.btSummary.Click += new System.EventHandler(this.btSummary_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(6, 97);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(118, 20);
            this.dateTimePicker1.TabIndex = 0;
            // 
            // rbStock
            // 
            this.rbStock.AutoSize = true;
            this.rbStock.Location = new System.Drawing.Point(14, 65);
            this.rbStock.Name = "rbStock";
            this.rbStock.Size = new System.Drawing.Size(53, 17);
            this.rbStock.TabIndex = 2;
            this.rbStock.Text = "Stock";
            this.rbStock.UseVisualStyleBackColor = true;
            // 
            // rbSaleByDay
            // 
            this.rbSaleByDay.AutoSize = true;
            this.rbSaleByDay.Location = new System.Drawing.Point(14, 42);
            this.rbSaleByDay.Name = "rbSaleByDay";
            this.rbSaleByDay.Size = new System.Drawing.Size(80, 17);
            this.rbSaleByDay.TabIndex = 1;
            this.rbSaleByDay.Text = "Sale by day";
            this.rbSaleByDay.UseVisualStyleBackColor = true;
            // 
            // rbProdByDay
            // 
            this.rbProdByDay.AutoSize = true;
            this.rbProdByDay.Checked = true;
            this.rbProdByDay.Location = new System.Drawing.Point(14, 19);
            this.rbProdByDay.Name = "rbProdByDay";
            this.rbProdByDay.Size = new System.Drawing.Size(110, 17);
            this.rbProdByDay.TabIndex = 0;
            this.rbProdByDay.TabStop = true;
            this.rbProdByDay.Text = "Production by day";
            this.rbProdByDay.UseVisualStyleBackColor = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.homeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(718, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // homeToolStripMenuItem
            // 
            this.homeToolStripMenuItem.Name = "homeToolStripMenuItem";
            this.homeToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.homeToolStripMenuItem.Text = "Home";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(718, 353);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Main";
            this.Text = "Main";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Main_FormClosed);
            this.tabControl.ResumeLayout(false);
            this.tabProdOrder.ResumeLayout(false);
            this.gbProdOrderAction.ResumeLayout(false);
            this.gbProdOrderAction.PerformLayout();
            this.gbAction.ResumeLayout(false);
            this.gbAction.PerformLayout();
            this.tabSale.ResumeLayout(false);
            this.tabSale.PerformLayout();
            this.tabActionTrail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgActionTrail)).EndInit();
            this.tabSummaryReports.ResumeLayout(false);
            this.gbReportData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSummary)).EndInit();
            this.gbReportType.ResumeLayout(false);
            this.gbReportType.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabProdOrder;
        private System.Windows.Forms.TabPage tabSale;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem homeToolStripMenuItem;
        private System.Windows.Forms.GroupBox gbAction;
        private System.Windows.Forms.TabPage tabActionTrail;
        private System.Windows.Forms.TabPage tabSummaryReports;
        private System.Windows.Forms.GroupBox gbProdOrderAction;
        private System.Windows.Forms.Button btSubmitProdORder;
        private System.Windows.Forms.TextBox tbProdORderMachineNo;
        private System.Windows.Forms.TextBox tbProdORderPRodSpan;
        private System.Windows.Forms.TextBox tbProdORderQProduced;
        private System.Windows.Forms.ComboBox cbProdORderUnit;
        private System.Windows.Forms.ComboBox cbProdORderProduct;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbAddEntry;
        private System.Windows.Forms.ComboBox cbSaleProduction;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbCompany;
        private System.Windows.Forms.ComboBox cbMarket;
        private System.Windows.Forms.Button btSubmitSale;
        private System.Windows.Forms.TextBox tbSaleQuantity;
        private System.Windows.Forms.ComboBox cbSaleUnit;
        private System.Windows.Forms.ComboBox cbSaleProduct;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DataGridView dgActionTrail;
        private System.Windows.Forms.GroupBox gbReportType;
        private System.Windows.Forms.RadioButton rbProdByDay;
        private System.Windows.Forms.RadioButton rbStock;
        private System.Windows.Forms.RadioButton rbSaleByDay;
        private System.Windows.Forms.TextBox tbPricePerUnit;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tbAvailable;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox gbReportData;
        private System.Windows.Forms.Button btSummary;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DataGridView dgvSummary;
        private System.Windows.Forms.Label quantityerror;
    }
}