﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.SqlServer.Server;
using DataAccess;

namespace CMSProject
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
            cbRoles.SelectedIndex = 0;
        }


        private void cbRoles_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cbRoles.SelectedIndex)
            {
                case 0:
                {
                    tbUsername.Text = "ProductionOperaterF";
                    tbPassword.Text = "ProductionOperaterF";
                    break;
                }

                case 1:
                {

                    tbUsername.Text = "OperatorSupervisorF";
                    tbPassword.Text = "OperatorSupervisorF";
                    break;
                }

                case 2:
                {

                    tbUsername.Text = "SeniorManagementF";
                    tbPassword.Text = "SeniorManagementF";
                    break;
                }
            }
        }

        private void btEnter_Click(object sender, EventArgs e)
        {
            string role = "";

            switch (cbRoles.SelectedIndex)
            {
                case 0:
                {
                    role = Roles.ProductionOperator;
                    break;
                }

                case 1:
                {
                    role = Roles.OperatorSupervisor;
                    break;
                }

                case 2:
                {
                    role = Roles.SeniorManagement;
                    break;
                }
            }

            Main mainForm = new Main(cbRoles.SelectedIndex, new DataAccess.DataAccess(role, tbServer.Text, tbDatabase.Text));
            this.Hide();
            mainForm.ShowDialog();

        }

    }
}
