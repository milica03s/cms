﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataAccess;

namespace CMSProject
{
    public partial class Main : Form
    {
        private object selectedItem;
        private DataAccess.DataAccess dataAccess;

        public Main()
        {
            InitializeComponent();
        }

        public Main(int selectedItem, DataAccess.DataAccess dataAccess)
        {
            InitializeComponent();
            this.dataAccess = dataAccess;
            PopulateData(selectedItem);

        }

        private void PopulateData(int selectedRole)
        {
            switch (selectedRole)
            {
                case 0:
                {
                    tabControl.SelectedIndex = 0;
                    PopulateProductionOrder();
                    break;
                }

                case 1:
                {
                    tabControl.SelectedIndex = 1;
                    PopulateSale();
                    break;
                }

                case 2:
                {
                    tabControl.SelectedIndex = 3;
                    break;
                }

                default:
                {
                    tabControl.SelectedIndex = 0;
                    PopulateProductionOrder();
                    break;
                }
            }
        }

        private void PopulateProductionOrder()
        {
            var predefinedData = dataAccess.GetAvailableData();

            cbProdORderProduct.DisplayMember = "NAME";
            cbProdORderProduct.ValueMember = "ID";
            cbProdORderProduct.DataSource = predefinedData[0];

            cbProdORderUnit.DisplayMember = "NAME";
            cbProdORderUnit.ValueMember = "ID";
            cbProdORderUnit.DataSource = predefinedData[1];

            tbProdORderMachineNo.Text = "";
            tbProdORderPRodSpan.Text = "";
            tbProdORderQProduced.Text = "";
        }

        private void PopulateSale()
        {
            var predefinedData = dataAccess.GetAvailableData(true);

            cbSaleProduct.DisplayMember = "NAME";
            cbSaleProduct.ValueMember = "ID";
            cbSaleProduct.DataSource = predefinedData[0];

            cbSaleUnit.DisplayMember = "NAME";
            cbSaleUnit.ValueMember = "ID";
            cbSaleUnit.DataSource = predefinedData[1];

            cbMarket.DisplayMember = "NAME";
            cbMarket.ValueMember = "ID";
            cbMarket.DataSource = predefinedData[2];

            cbCompany.DisplayMember = "NAME";
            cbCompany.ValueMember = "ID";
            cbCompany.DataSource = predefinedData[3];

            populateProductionData(0,
                Convert.ToInt32(cbSaleProduct.SelectedValue),
                Convert.ToInt32(cbSaleUnit.SelectedValue));


        }


        private void btSubmitProdORder_Click(object sender, EventArgs e)
        {
            int data = dataAccess.SaveProductionOrderData(
                Convert.ToInt32(cbProdORderProduct.SelectedValue),
                Convert.ToInt32(cbProdORderUnit.SelectedValue),
                Convert.ToInt32(tbProdORderQProduced.Text),
                tbProdORderPRodSpan.Text,
                tbProdORderMachineNo.Text
            );

            PopulateProductionOrder();

        }

        private void tabSale_Click(object sender, EventArgs e)
        {

        }

        private void cbMarket_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbCompany.DisplayMember = "NAME";
            cbCompany.ValueMember = "ID";
            cbCompany.DataSource = dataAccess.GetCompanies(Convert.ToInt32(cbMarket.SelectedValue));
        }

        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (tabControl.SelectedIndex)
            {
                case 0:
                {
                    PopulateProductionOrder();
                    break;
                }

                case 1:
                {
                    PopulateSale();
                    break;
                }

                case 2:
                {
                    PopulateActionTrail();
                        break;
                }

                default:
                {
                    PopulateProductionOrder();
                    break;
                }
            }

        }


        private void PopulateActionTrail()
        {
            var data = dataAccess.GetActionTrailData();

            dgActionTrail.DataSource = data;

        }

        private void btSubmitSale_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(tbAvailable.Text) < Convert.ToInt32(tbSaleQuantity.Text))
            {
                quantityerror.Visible = true;
                quantityerror.Text = "You entered the worng quantity to sell";
                return;
            }

            quantityerror.Visible = false;

            var m = dataAccess.SaveSale(
                Convert.ToInt32(cbSaleProduct.SelectedValue),
                Convert.ToInt32(cbSaleUnit.SelectedValue),
                tbPricePerUnit.Text,
                Convert.ToInt32(cbSaleProduction.Text),
                Convert.ToInt32(tbSaleQuantity.Text),
                Convert.ToInt32(cbCompany.SelectedValue)
            );

            PopulateSale();
        }

        private void cbSaleProduction_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbSaleProduction.SelectedValue != null)
            {
                tbAvailable.Text = cbSaleProduction.SelectedValue.ToString();
            }
            else
            {
                tbAvailable.Text = "";
            }
        }

        private void cbSaleUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            populateProductionData(0,
                Convert.ToInt32(cbSaleProduct.SelectedValue),
                Convert.ToInt32(cbSaleUnit.SelectedValue));
        }

        private void cbSaleProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            populateProductionData(0,
                Convert.ToInt32(cbSaleProduct.SelectedValue),
                Convert.ToInt32(cbSaleUnit.SelectedValue));

        }

        private void populateProductionData(int i, int fk_product_id, int fk_unit_of_measure_id)
        {
            if (cbSaleProduction.Items.Count != 0)
            {
                cbSaleProduction.DataSource = null;
                //cbSaleProduction.Items.Clear();
            }

            var productOrders = dataAccess.GetProductionOrders(i, fk_product_id, fk_unit_of_measure_id);

            cbSaleProduction.DisplayMember = "ID";
            cbSaleProduction.ValueMember = "AVAILABLE";
            cbSaleProduction.DataSource = productOrders;
        }

        private void btSummary_Click(object sender, EventArgs e)
        {
            if (rbProdByDay.Checked)
            {
                var productionOrdersByDate = dataAccess.GetProductionOrders(dateTimePicker1.Value.ToLongDateString());
                dgvSummary.DataSource = productionOrdersByDate;
                return;
            }

            if (rbSaleByDay.Checked)
            {
                var saleByDate = dataAccess.GetSale(dateTimePicker1.Value.ToLongDateString());
                dgvSummary.DataSource = saleByDate;
                return;
            }

            if (rbStock.Checked)
            {
                var stockData = dataAccess.GetStock(dateTimePicker1.Value.ToLongDateString());
                dgvSummary.DataSource = stockData;
                return;
            }
        }

        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
